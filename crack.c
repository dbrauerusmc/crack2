#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"
int file_length(char *filename);

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
    char *hashguess = md5(guess, strlen(guess));

    // Compare the two hashes
    if (strcmp(hashguess, hash) == 0)
    {
        free(hashguess);
        return 1;
    }
    else
    {
        free(hashguess);
        return 0;
    }

    // Free any malloc'd memory

}

// Read in the dictionary file and return the array of strings
// and store the length of the array in size.
// This function is responsible for opening the dictionary file,
// reading from it, building the data structure, and closing the
// file.
char **read_dictionary(char *filename, int *size)
{

    
        
        int len = file_length(filename);
        char *fruit = malloc(len);
        
        //read file into fruit memory
        FILE *f = fopen(filename, "r");
        if (!f)
        {
            printf("Can't open %s for reading\n", filename);
            exit(1);
        }    
        fread(fruit, sizeof(char), len, f);
        fclose(f);
        
        //Change newlines into nulls and count them as we go.
        int num_fruits = 0;
        
        for (int i = 0; i < len; i++)
        {
            if (fruit[i] == '\n')
            {
                fruit[i] = '\0';
                num_fruits++;
            }
        }
        
        
        
        //Allocate space for array of pointers  <-- because we need another array that points to the start of each one of them
        char **fruits = malloc(num_fruits * sizeof(char *));
        
        //Load array with addresses of each fruit
        fruits[0] = &fruit[0];
        int j = 1;
        for (int i = 0; i < len-1; i++)
        {
            if (fruit[i] == '\0')
            {
                fruits[j] = &fruit[i+1];
                j++;
            }
        }
        
        //Print them out
      //  for (int i = 0; i < num_fruits; i++)
     //   {
     //       printf("%d %s\n", i, fruits[i]);
     //   }

    
    
    *size = num_fruits;  // go to this address *size and put this value num_fruits in there.
    return fruits;
}

    int file_length(char *filename)
    {
        struct stat info;
        int ret = stat(filename, &info);
        if (ret == -1) return -1;
        else return info.st_size;
        
    }


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }
//tryguess("827ccb0eea8a706c4c34a16891f84e7b", "ushdcuh");
    // Read the dictionary file into an array of strings.
    int dlen;
    char **dict = read_dictionary(argv[2], &dlen);
    

    // Open the hash file for reading.
        //read file into fruit memory
        FILE *f = fopen(argv[1], "r");
        if (!f)
        {
            printf("Can't open %s for reading\n", argv[1]);
            exit(1);
        }   
    
    
    

    // For each hash, try every entry in the dictionary.
    // Print the matching dictionary entry.
    // Need two nested loops..
    /*
    char * line = NULL;
    ssize_t hashes;
    size_t len = 0;
    
    while ((hashes = getline(&line, &len, f)) != -1) 
    {
      //  printf("Retrieved line of length %zu :\n", read);
       // printf("%s", line);
        for (int i = 0; i < dlen; i++)
        {
            if (tryguess(line, *dict) == 1)
            {
                printf("%c", *dict[i]);
                
                printf("%s %zu\n", dict[i], hashes);
            }
        }
       
        
        
    }

    fclose(f);
    
    */
    
    char hashes[HASH_LEN];

    while(fgets(hashes,HASH_LEN,f) != NULL)
    {

        for(int i = 0; i < dlen; i++)
        {

            //printf("%s %s\n",dict[i], hashes);

            if(tryguess(hashes,dict[i]) == 1)
            {

                printf("%s %s\n",dict[i],hashes);

            }

        }
    }
}
